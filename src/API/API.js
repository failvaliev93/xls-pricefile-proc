const fs = require('fs');
const path = require("path");
const { cleanerIntervalInHour } = require('../config');
const FileReader = require('./FileReader');
const Initialiser = require('./Initialiser');
const Journal = require('./Journal');
const Settings = require('./Settings');


const folderPathUpload = path.join(__dirname, '../../storage/uploads/');
const folderPathSucces = path.join(__dirname, '../../storage/success/');
const folderPathInProgress = path.join(__dirname, '../../storage/inProgress/');
const folderPathErrors = path.join(__dirname, '../../storage/errors/');

const dayMS = 86400000;

const StatusText = {
  uploaded: 'uploaded',
  inProgress: 'in progress',
  error: 'error',
  success: 'succes'
}

class API {
  constructor() {
    const initialiser = new Initialiser();
    initialiser.createFolders();

    this.journal = new Journal();
    this.settings = new Settings();

    setInterval(() => this.clearOldFilesAndDB(), cleanerIntervalInHour * dayMS / 24);
  }

  async registerFile(fileName) {
    return await this.journal.write(fileName, StatusText.uploaded);
  }

  async fileProcessing(workerElement) {
    try {
      const fileName = workerElement.name;

      //to inProgress
      fs.rename(folderPathUpload + fileName, folderPathInProgress + fileName,
        async err => {
          if (err) throw err;

          const filePath = folderPathInProgress + fileName;
          const reader = new FileReader(filePath, this.getCurrentSettings());
          await reader.startProc();

          if (reader.result) {
            //to success
            fs.rename(folderPathInProgress + fileName, folderPathSucces + fileName, err => {
              if (err) throw err;
              this.journal.update({
                id: workerElement.id,
                status: StatusText.success,
                result: reader.result,
              })
            });

          } else {
            // to error
            fs.rename(folderPathInProgress + fileName, folderPathErrors + fileName, err => {
              if (err) throw err;
              this.journal.update({
                id: workerElement.id,
                status: StatusText.error
              });
            });
          }
        });


    } catch (error) {
      console.log('fileProcessing:', error);
    }
  }

  async status(id) {
    return await this.journal.get(id);
  }

  async setStatusInProgress(id) {
    return await this.journal.update({ id, status: StatusText.inProgress });
  }

  getCurrentSettings() {
    return this.settings.getCurrent()
  }

  getDefaultSettings() {
    return this.settings.getDefault()
  }

  setCurrentSettings(currentSettings) {
    this.settings.setCurrent(currentSettings);
  }

  setCurrentSettingsFromDefault() {
    this.setCurrentSettings({ ...this.getDefaultSettings() });
  }

  clearOldFilesAndDB() {
    console.log('cleaner started...');
    const records = this.journal.getAll();
    const now = new Date();
    const today = new Date(now.getFullYear(), now.getMonth(), now.getDate());

    if (records && records.length) {
      for (let index = 0; index < records.length; index++) {
        const we = records[index];
        const dateCreated = new Date(we.dateCreated);
        // раньше чем вчера
        if (dateCreated.valueOf() < today.valueOf() - dayMS) {
          const name = we.name;
          if (we.status === 'succes') {
            deleteFile(folderPathSucces + name);
          } else if (we.status === 'uploaded') {
            deleteFile(folderPathUpload + name);
          } else if (we.status === 'error') {
            deleteFile(folderPathErrors + name);
          }

          this.journal.delete(we.id);
          console.log('Success deleted:', name);
        }
      }
    }

    function deleteFile(path) {
      try { fs.unlinkSync(path) } catch (error) { }
    }
    console.log('cleaner completed');
  }
}

module.exports = { API, StatusText }