const fs = require('fs');
const path = require("path");
const axios = require('axios');
const FormData = require('form-data');

const sandbox_key = require('./key.js');
const orig_key = require('./key2.js');
const key = orig_key;
const url = 'https://api.sandbox.cloudconvert.com'
const URL_CONVERT = url + '/v2/convert'
const URL_IMPORT = url + '/v2/import/url'
const URL_UPLOAD_TASK = url + '/v2/import/upload'

class Converter {
  constructor(inputFile) {
    this.uploadTaskRes;

    this.createUploadTask();
  }

  createUploadTask() {
    axios({
      method: "post",
      url: URL_UPLOAD_TASK,
      headers: { 'Content-Type': `application/json` },
    })
      .then(res => {
        this.uploadTaskRes = res.data;
        console.log(res.data);
      })
      .catch(error => {
        this.status = { status: "error", message: `Send file error` };
        console.error('error createUploadTask:', error);
        this.errorCallback();
      })
  }

  sendFIle(inputFile) {
    let fd = new FormData();
    try {
      //form-data error if file undefined
      fd.append("FileData", zipFile);
    } catch (error) {
      console.log("no file");
      console.log(error);
    }

    axios({
      method: "post",
      url: URL_IMPORT,
      data: fd,
      headers: { 'Content-Type': `multipart/form-data; boundary=${fd._boundary}` },
    })
      .then(res => {
        this.status = res.data;
        console.log('Send file: ', res.data);

        if (res.data.status === 'success') {
          let creativeid = res.data.creativeid;
          this.convertGif(creativeid);
        } else {
          this.errorCallback();
        }
      })
      .catch(error => {
        this.status = { status: "error", message: `Send file error` };
        console.error(error);
        this.errorCallback();
      })
  }

  errorCallback() { }
}

module.exports = Converter