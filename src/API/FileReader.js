const { getInfoFromFilePath } = require("./helpers");
const XlsProc = require("./XlsProc");
const Validation = require("./Validation");
const InfoCollector = require("./InfoCollector");
const { unitValueWords } = require('../config.js');

const Extensions = {
  XLS: 'XLS',
  XLSX: 'XLSX',
}

class FileReader {
  constructor(filePath, settings) {
    this.filePath = filePath;
    this.result;
    this.settings = settings;
    this.infoCollector = new InfoCollector();
  }

  async startProc() {
    let { ext } = getInfoFromFilePath(this.filePath);
    ext = ext.toUpperCase();

    if (ext === Extensions.XLS) {
      this.XLSProc();
    } else if (ext === Extensions.XLSX) {
      this.XLSXProc();
    }
  }

  XLSProc() {
    try {
      const proc = new XlsProc(this.filePath, { codepage: '1251' }, this.settings.startOfTable, this.settings.endOfTable, this.settings.cellAdrStart);
      this.extract(proc.collector.collection);
    } catch (error) {
      console.log('FileReader.XLSProc:', error);
    }
  }

  XLSXProc() {
    try {
      const proc = new XlsProc(this.filePath, null, this.settings.startOfTable, this.settings.endOfTable, this.settings.cellAdrStart);
      this.extract(proc.collector.collection);
    } catch (error) {
      console.log('FileReader.XLSProc:', error);
    }
  }

  extract(collection) {
    let artColN = undefined;
    let productColN = undefined;
    let countColN = undefined;
    let priceColN = undefined;
    let unitColN = undefined;
    let commentColN = undefined;

    let rowSkip = false;
    let nextColAfterCount; //"замок" для поиска безымянного unit

    for (let i = 0; i < collection.length; i++) {
      const row = collection[i];
      const currentRowN = row.row;

      if (row?.value?.length) {
        for (let j = 0; j < row.value.length; j++) {
          const cell = row.value[j];
          const { col, value } = cell;
          const currentColumn = col;

          if (value) {
            //загаловок
            if (i === 0) {
              //ед.изм. нет, но надо проверить в след. столбце после кол-ва
              if (countColN && !nextColAfterCount) nextColAfterCount = currentColumn;

              if (!productColN && Validation.searchIn(value, this.settings.productHeader)) {
                productColN = currentColumn;
                continue;
              } else if (!artColN && Validation.searchIn(value, this.settings.artikulHeader)) {
                artColN = currentColumn;
                continue;
              } else if (!countColN && Validation.searchIn(value, this.settings.countHeader)) {
                countColN = currentColumn;
                continue;
              } else if (Validation.searchIn(value, this.settings.priceHeader)) {
                //последний подходящий
                priceColN = currentColumn;
                continue;
              } else if (!unitColN && Validation.searchIn(value, this.settings.unitHeader)) {
                unitColN = currentColumn;
                continue;
              }

            } else {
              //тело
              if (!rowSkip) {
                if (productColN && productColN === currentColumn) {
                  if (value && value.length === 1) {
                    this.infoCollector.rejectPart();
                    rowSkip = true;
                  } else {
                    this.infoCollector.savePartially({ product: value });
                  }

                } else if (artColN && artColN === currentColumn) {
                  this.infoCollector.savePartially({ artikul: value })

                } else if (countColN && countColN === currentColumn) {
                  this.infoCollector.savePartially({ count: value });

                  //установка ед.изм сразу после кол-ва
                  if (!unitColN && nextColAfterCount) {
                    const val = row.value[j + 1]?.value;
                    if (val && Validation.compare(val, unitValueWords)) {
                      unitColN = row.value[j + 1].col;
                      this.infoCollector.savePartially({ unit: val })
                    } else {
                      nextColAfterCount = undefined;
                    }
                  }

                } else if (priceColN && priceColN === currentColumn) {
                  this.infoCollector.savePartially({ price: value })

                } else if (unitColN && unitColN === currentColumn) {
                  this.infoCollector.savePartially({ unit: value })
                }
              }
            }
          }
        }
        if (rowSkip) {
          rowSkip = false;
          this.infoCollector.rejectPart();
        } else {
          this.infoCollector.addFromPart();
        }
      }
    }

    this.result = this.infoCollector.collection
  }
}

module.exports = FileReader