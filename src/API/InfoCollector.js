
class InfoCollector {
  constructor() {
    this._product;
    this._count;
    this._price;
    this._unit;
    this._artikul;

    this.collection = [];
  }

  add(product, count, price, unit, artikul) {
    this.collection.push({
      product: product,
      count: count,
      price: price,
      unit: unit,
      artikul: artikul,
    })
  }

  savePartially({
    product,
    count,
    price,
    unit,
    artikul,
  }) {
    if (product) this._product = product;
    if (count) this._count = count;
    if (price) this._price = price;
    if (unit) this._unit = unit;
    if (artikul) this._artikul = artikul;
  }

  addFromPart() {
    if (!this._product || (!this._count && !this._price && !this._unit && !this.artikul))
      return this.rejectPart();

    this.add(
      this._product,
      this._count,
      this._price,
      this._unit,
      this._artikul,
    );
    this.rejectPart();
  }

  rejectPart() {
    this._product = undefined;
    this._count = undefined;
    this._price = undefined;
    this._unit = undefined;
    this._artikul = undefined;
  }
}

module.exports = InfoCollector