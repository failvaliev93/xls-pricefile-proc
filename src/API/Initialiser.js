const fs = require('fs');
const path = require("path");

class Initialiser {
  constructor() { }

  createFolders() {
    this._createFolder(path.join(__dirname, `../../storage`));
    this._createFolder(path.join(__dirname, `../../storage/errors`));
    this._createFolder(path.join(__dirname, `../../storage/inProgress`));
    this._createFolder(path.join(__dirname, `../../storage/success`));
    this._createFolder(path.join(__dirname, `../../storage/uploads`));
  }

  _createFolder(folderPath) {
    if (!fs.existsSync(folderPath)) {
      fs.mkdirSync(folderPath);
    }
  }
}

module.exports = Initialiser