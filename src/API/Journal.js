const { JsonDB } = require('node-json-db');
const { Config } = require('node-json-db/dist/lib/JsonDBConfig');
const { v1: uuidv1 } = require('uuid');

class WorkElement {
  constructor(name) {
    this.dateCreated = new Date();
    this.name = name;
  }
  id = uuidv1();
  status = '';
  result = '';
}

class Journal {
  constructor() {
    this.db = new JsonDB(new Config(
      "FilesDB",
      true, //autosave 
      true, //human readable
      '/' //separator
    ));
  }

  write(fileName, status = '') {
    const we = new WorkElement(fileName);
    we.status = status;
    this.db.push('/workElements[]', we)
    return we
  }

  update({ id, status, result }) {
    const index = this.db.getIndex('/workElements', id);

    if (index > -1) {
      if (status) this.db.push(`/workElements[${index}]/status`, status);
      if (result) this.db.push(`/workElements[${index}]/result`, result);

      return this.db.getData(`/workElements[${index}]`)
    } else {
      return undefined
    }
  }

  delete(id) {
    const index = this.db.getIndex('/workElements', id);
    if (index > -1) {
      this.db.delete(`/workElements[${index}]`);
    }
  }

  clear() {
    this.db.push('/workElements', []);
  }

  get(id) {
    const index = this.db.getIndex('/workElements', id);
    if (index > -1) {
      return this.db.getData(`/workElements[${index}]`);
    }
    return undefined
  }

  getAll() {
    return this.db.getData(`/workElements`);
  }
}

module.exports = Journal