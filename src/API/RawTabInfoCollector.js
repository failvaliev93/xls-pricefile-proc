
class RawTabInfoCollector {
  constructor() {
    this.collection = [];
    this.counterRow = 0;
    this.counterCol = 0;
  }

  addRow(valueList, rowNum) {
    // this.counterRow++;
    this.counterRow = rowNum;
    this.counterCol = 0;
    this.collection.push({ row: this.counterRow, value: valueList });
  }

  addNewRow(rowNum) {
    this.addRow([], rowNum);
  }

  addCell(cellName, value, colNum) {
    // this.counterCol++;
    this.counterCol = colNum;
    this.collection[this.collection.length - 1].value.push({ cellName, value, row: this.counterRow, col: this.counterCol });
  }
}

module.exports = RawTabInfoCollector