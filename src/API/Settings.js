const { JsonDB } = require('node-json-db');
const { Config } = require('node-json-db/dist/lib/JsonDBConfig');

class SettingsInit {
  startOfTable = 'ТОВАР, изделие, №, Наименование товара'
  endOfTable = 'ИТОГО, Общий вес, Вес нетто'
  artikulHeader = 'АРТИКУЛ, арт., код'
  productHeader = 'ТОВАР'
  countHeader = 'КОЛ-ВО, количество'
  unitHeader = 'ЕД., единица'
  priceHeader = 'цена'
  cellAdrStart = '22'
}

class Settings {
  constructor() {
    this.db = new JsonDB(new Config(
      "SettingsDB",
      true, //autosave 
      true, //human readable
      '/' //separator
    ));

    try {
      this.db.getData('/current')
    } catch (error) {
      this.db.push('/current', new SettingsInit)
    }

    try {
      this.db.getData('/default')
    } catch (error) {
      this.db.push('/default', new SettingsInit)
    }
  }

  getDefault() {
    return this.db.getData('/default')
  }

  getCurrent() {
    return this.db.getData('/current')
  }

  setCurrent({
    startOfTable = '',
    endOfTable = '',
    artikulHeader = '',
    productHeader = '',
    countHeader = '',
    unitHeader = '',
    priceHeader = '',
    cellAdrStart = '',
  }) {
    if (startOfTable) this.db.push('/current/startOfTable', startOfTable);
    if (endOfTable) this.db.push('/current/endOfTable', endOfTable);
    if (artikulHeader) this.db.push('/current/artikulHeader', artikulHeader);
    if (productHeader) this.db.push('/current/productHeader', productHeader);
    if (countHeader) this.db.push('/current/countHeader', countHeader);
    if (unitHeader) this.db.push('/current/unitHeader', unitHeader);
    if (priceHeader) this.db.push('/current/priceHeader', priceHeader);
    if (cellAdrStart) this.db.push('/current/cellAdrStart', cellAdrStart);
  }
}

module.exports = Settings