
function str2map(str) {
  return str.split(',').map(w => w.toString().trim().toUpperCase()).filter(Boolean);
}

/**
 * @words : string, 'ИТОГ, Общий вес, Вес нетто, оплате"
 * @cellValue : string, "итого"
 */
class Validation {
  //точное сравнение
  static compare(cellValue, words) {
    const arr = str2map(words);
    return arr.includes(cellValue.toString().toUpperCase().trim())
  }

  static searchIn(cellValue, words) {
    const arr = str2map(words);
    cellValue = cellValue.toString().toUpperCase();

    for (let i = 0; i < arr.length; i++) {
      const word = arr[i];
      if (cellValue.indexOf(word) > -1) return true
    }

    return false
  }
}

module.exports = Validation