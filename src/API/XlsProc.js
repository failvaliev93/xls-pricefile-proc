const fs = require('fs');
const path = require("path");
const XLSX = require("xlsx");
const RawTabInfoCollector = require('./RawTabInfoCollector');
const Validation = require('./Validation');


class XlsProc {
  constructor(file, params = {}, startOfTable, endOfTable, cellAdrStart) {
    this.xls;
    this.startOfTable = startOfTable;
    this.endOfTable = endOfTable;
    this.cellAdrStart = cellAdrStart;

    this.fileReadParams = {};
    this.fileReadParams.bookFiles = false; //If true, add raw files to book object 
    this.fileReadParams.bookDeps = false; //If true, parse calculation chains
    if (params?.codepage) this.fileReadParams.codepage = params.codepage;

    this.collector = new RawTabInfoCollector();

    this.read(file);
    this.searchTarget();
  }

  read(file) {
    try {
      const p = {
        bookFiles: true,
        bookDeps: true,
        codepage: '1251'
      };

      // console.log('XlsProc: start reading ', file);

      this.xls = XLSX.readFile(file, this.fileReadParams);
    } catch (error) {
      console.log(error);
    }
  }

  writeJSON(jsonPath) {
    if (!jsonPath) jsonPath = path.join(__dirname, `../../debug/xls.json`);
    let outStrJSON = JSON.stringify(this.xls);
    fs.writeFile(jsonPath, outStrJSON, 'utf-8', err => {
      if (err) return console.log(err);
      console.log("JSON file has been saved.");
    })
  }

  searchTarget() {
    if (this.xls) {
      let succesFirstAlgorithm = false;
      const cells = [];

      let startList = this.cellAdrStart;
      if (startList) {
        startList.split(',').map(Boolean).map(Number);
      }
      // console.log(startList);

      //поиск по адресу
      if (startList && startList.length) {
        let startTargetTable = false;
        let successFind = false;

        for (const sheetName in this.xls.Sheets) {
          if (Object.hasOwnProperty.call(this.xls.Sheets, sheetName)) {
            for (const cellName in this.xls.Sheets[sheetName]) {
              if (Object.hasOwnProperty.call(this.xls.Sheets[sheetName], cellName)) {
                const cell = this.xls.Sheets[sheetName][cellName];
                if (cell.v) {
                  const row = +this._getNumber(cellName);

                  if (!successFind) {
                    if (startList.includes(row)) {
                      startTargetTable = true;
                    } else {
                      startTargetTable = false;
                    }
                  }

                  const value = cell.v.toString().toUpperCase();
                  //начало таблицы
                  if (!successFind && (startTargetTable && Validation.compare(value, this.startOfTable))) {
                    successFind = true;
                    // if (cell.v) cells.push({ cellName: cellName, value: cell?.v })
                  }

                  //конец таблицы
                  if (successFind && Validation.searchIn(value, this.endOfTable)) {
                    startTargetTable = false;
                    successFind = false;
                    break;
                  }

                  if (successFind) {
                    cells.push({ cellName: cellName, value: cell?.v });
                  }
                }
              }
            }
          }
        }
        if (cells.length) succesFirstAlgorithm = true;
        console.log('succesFirstAlgorithm:', succesFirstAlgorithm);
        // console.log(cells);
      }

      //поиск по совпадению
      if (!succesFirstAlgorithm) {
        let startTargetTable = false;
        for (const sheetName in this.xls.Sheets) {
          if (Object.hasOwnProperty.call(this.xls.Sheets, sheetName)) {
            for (const cellName in this.xls.Sheets[sheetName]) {
              if (Object.hasOwnProperty.call(this.xls.Sheets[sheetName], cellName)) {
                const cell = this.xls.Sheets[sheetName][cellName];

                if (cell.v) {
                  const value = cell.v.toString().toUpperCase();
                  //конец таблицы
                  if (startTargetTable && Validation.searchIn(value, this.endOfTable)) {
                    // console.log(value, this.endOfTable);
                    startTargetTable = false;
                    break;
                  }

                  //начало таблицы
                  if (Validation.compare(value, this.startOfTable) || startTargetTable) {
                    startTargetTable = true;
                    if (cell.v) cells.push({ cellName: cellName, value: cell?.v })
                  }
                }
              }
            }
          }
        }
      }

      if (cells.length) {
        // this.extractInfo(cells);
        this.prepare(cells);
      }
    } else {
      throw new Error('no such file')
    }
  }

  prepare(cells) {
    //буквы ячеек в цифры
    let abc = {};
    cells.forEach(cell => { abc[this._getStr(cell.cellName)] = -1 });
    Object.keys(abc).sort(function (a, b) {
      return a.length - b.length || a.localeCompare(b)
    }).map((v, i) => abc[v] = i + 1);

    let prevRowN = undefined;
    let colN = 1;

    for (let i = 0; i < cells.length; i++) {
      const { cellName, value } = cells[i];
      const currentrowN = +this._getNumber(cellName);

      if (prevRowN != currentrowN) {
        this.collector.addNewRow(currentrowN);
        colN = 1;
      }

      this.collector.addCell(cellName, value, abc[this._getStr(cellName)]);

      colN++;
      prevRowN = currentrowN;
    }

    // console.log(this.collector.collection);

    // let jsonPath = path.join(__dirname, `../../debug/collector.json`);
    // let outStrJSON = JSON.stringify(this.collector.collection);
    // fs.writeFile(jsonPath, outStrJSON, 'utf-8', err => {
    //   if (err) return console.log(err);
    //   console.log("JSON file has been saved.");
    // })
  }

  extractInfo(cells) {
    let prevRowN = undefined;
    let tabHeaderRowN = undefined;

    let artColN = undefined;
    let productColN = undefined;
    let countColN = undefined;
    let priceColN = undefined;
    let unitColN = undefined;
    let commentColN = undefined;

    let rowSkip = false;
    let nextColAfterCount; //"замок" для поиска безымянного unit
    let tryValidUnit

    for (let i = 0; i < cells.length; i++) {
      const { cellName, value } = cells[i];
      const currentRowN = this._getNumber(cellName);
      const currentColumn = this._getStr(cellName);

      if (!tabHeaderRowN) tabHeaderRowN = currentRowN;

      //новая строка
      if (prevRowN && prevRowN != currentRowN) {
        if (rowSkip) {
          rowSkip = false
        } else {
          this.infoCollector.addFromPart();
        }
      }

      //загаловок
      if (tabHeaderRowN === currentRowN) {
        if (countColN && !nextColAfterCount) nextColAfterCount = currentColumn;

        if (!productColN && Validation.searchIn(value, this.settings.productHeader)) {
          productColN = currentColumn;
          continue;
        } else if (!artColN && Validation.searchIn(value, this.settings.artikulHeader)) {
          artColN = currentColumn;
          continue;
        } else if (!countColN && Validation.searchIn(value, this.settings.countHeader)) {
          countColN = currentColumn;
          continue;
        } else if (!priceColN && Validation.searchIn(value, this.settings.priceHeader)) {
          priceColN = currentColumn;
          continue;
        } else if (!unitColN && Validation.searchIn(value, this.settings.unitHeader)) {
          unitColN = currentColumn;
          continue;
        }

      } else {
        //тело
        if (this._isDataValid(value) && !rowSkip) {
          if (productColN && productColN === currentColumn) {
            if (value && value.length === 1) {
              this.infoCollector.rejectPart();
              rowSkip = true;
            } else {
              this.infoCollector.savePartially({ product: value });
            }

          } else if (artColN && artColN === currentColumn) {
            this.infoCollector.savePartially({ artikul: value })
          } else if (countColN && countColN === currentColumn) {
            this.infoCollector.savePartially({ count: value })
          } else if (priceColN && priceColN === currentColumn) {
            this.infoCollector.savePartially({ price: value })
          } else if (unitColN && unitColN === currentColumn) {
            this.infoCollector.savePartially({ unit: value })
          }

          //установка ед.изм сразу после кол-ва
          // if (!unitColN && artColN !== currentColumn && productColN !== currentColumn
          //   && countColN !== currentColumn && priceColN !== currentColumn && unitColN !== currentColumn
          // ) {
          if (!countColN && nextColAfterCount) {

          }
        }
      }

      prevRowN = currentRowN;
    }
    this.infoCollector.addFromPart();

    // console.log(this.infoCollector.collection);
  }

  _isDataValid = cellValue => Boolean(cellValue);
  _getNumber(cellName) {
    try {
      return cellName.match(/(-?\d+(\.\d+)?)/g).join('')
    } catch (error) {
      console.log(cellName);
    }
  }
  _getStr(cellName) { return cellName.replace(/[0-9]/g, "") }
}

module.exports = XlsProc