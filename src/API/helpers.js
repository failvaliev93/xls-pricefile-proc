

const getInfoFromFilePath = (filePath) => {
  try {
    const sp = filePath.split('\\');
    const f = sp[sp.length - 1];
    const dot = f.split('.');
    const ext = dot.pop();
    return {
      name: dot.join('.'),
      ext: ext,
      fullName: f,
    }
  } catch (error) {
    console.log('getInfoFromFilePath:', error);
    return {
      name: undefined,
      ext: undefined,
      fullName: undefined
    }
  }
}

module.exports = {
  getInfoFromFilePath,
}