const path = require("path");
const express = require('express');

const fs = require('fs');
const multer = require("multer");
const { API, StatusText } = require("./API/API");
const corsMiddleware = require('./cors.middleware');
const cors = require('cors');
const { sekretKey } = require('./config');
const { getInfoFromFilePath } = require('./API/helpers.js');

const api = new API();

const PORT = process.env.PORT || 8800;

const app = express();
app.use(express.json());
app.use(corsMiddleware);
app.use(cors());

const upload = multer({ dest: "storage/uploads" });
// app.use(multer({ dest: "uploads" }).single("filedata"));

//H: Content-Type:multipart/form-data
//B: filedata:<file>
app.post('/api/upload', upload.single("filedata"), async function (req, res) {
  try {

    if (!req.file) return res.status(400).send({
      message: "file needed",
      url: '/api/upload',
      status: 1
    });

    // const name = req.file.originalname;
    const fileInfo = getInfoFromFilePath(req.file.originalname)
    const name = req.file.filename + '.' + fileInfo.ext;

    const resStatus = await api.registerFile(name);

    await fs.rename(path.join(__dirname, `../${req.file.path}`),
      path.join(__dirname, `../${req.file.destination}/${name}`),
      () => { }
    );

    res.send({
      message: "succes",
      status: 0,
      url: '/api/upload',
      data: resStatus
    });
  } catch (error) {
    res.status(500).send({
      message: "error",
      url: '/api/upload',
      status: 1
    })
  }
})

//Q: id
app.get('/api/status', async function (req, res) {
  try {
    const { id } = req.query;

    if (!id) return res.status(400).send({
      message: "id needed",
      status: 1,
      url: '/api/status',
    });

    const resStatus = await api.status(id);

    if (!resStatus) return res.status(400).send({
      message: "not found",
      status: 1,
      url: '/api/status',
    });

    res.send({
      message: "succes",
      status: 0,
      url: '/api/status',
      data: resStatus
    });
  } catch (error) {
    res.status(500).send({
      message: "error",
      url: '/api/status',
      status: 1
    })
  }
})

//H: Content-Type:application/json
//B: id
app.post('/api/calc', async function (req, res) {
  try {
    const { id } = req.body;

    if (!id) return res.status(400).send({
      message: "id needed",
      status: 1,
      url: '/api/calc',
    });

    const prepareWorkerElement = await api.status(id);

    if (!prepareWorkerElement) return res.status(400).send({
      message: "not found",
      status: 1,
      url: '/api/calc',
    });

    if (prepareWorkerElement.status !== StatusText.uploaded) {
      return res.status(400).send({
        message: "файл уже обработан или в процессе",
        status: 1,
        url: '/api/calc',
      });
    }

    const workerElement = await api.setStatusInProgress(id);

    api.fileProcessing(workerElement);

    res.send({
      message: "succes",
      status: 0,
      data: workerElement,
      url: '/api/calc',
    });
  } catch (error) {
    res.status(500).send({
      message: "error",
      status: 1,
      url: '/api/calc',
    })
  }
})

app.get('/api/settings/current', async function (req, res) {
  try {
    const settings = await api.getCurrentSettings()
    res.send({
      message: "succes",
      status: 0,
      data: settings,
      url: '/api/settings/current',
    });
  } catch (error) {
    res.status(500).send({
      message: "error",
      status: 1,
      url: '/api/settings/current',
    })
  }
})

app.get('/api/settings/default', async function (req, res) {
  try {
    const settings = await api.getDefaultSettings()
    res.send({
      message: "succes",
      status: 0,
      data: settings,
      url: '/api/settings/default',
    });
  } catch (error) {
    res.status(500).send({
      message: "error",
      status: 1,
      url: '/api/settings/default',
    })
  }
})

//H: Content-Type:application/json
//B: key
app.post('/api/settings/current', async function (req, res) {
  try {
    const {
      key,
      startOfTable,
      endOfTable,
      artikulHeader,
      productHeader,
      countHeader,
      unitHeader,
      priceHeader,
      cellAdrStart,
    } = req.body;

    if (!key || key !== sekretKey) return res.status(400).send({
      message: "key wrong",
      status: 1,
      url: '/api/settings/current',
    });

    api.setCurrentSettings({
      startOfTable,
      endOfTable,
      artikulHeader,
      productHeader,
      countHeader,
      unitHeader,
      priceHeader,
      cellAdrStart,
    })

    res.send({
      message: "succes",
      status: 0,
      url: '/api/settings/current',
    });
  } catch (error) {
    res.status(500).send({
      message: "error",
      status: 1,
      url: '/api/settings/current',
    })
  }
})

//H: Content-Type:application/json
//B: key
app.post('/api/settings/current/default', async function (req, res) {
  try {
    const { key } = req.body;

    if (!key || key !== sekretKey) return res.status(400).send({
      message: "key wrong",
      status: 1,
      url: '/api/settings/current/default',
    });

    api.setCurrentSettingsFromDefault();

    res.send({
      message: "succes",
      status: 0,
      url: '/api/settings/current/default',
    });
  } catch (error) {
    res.status(500).send({
      message: "error",
      status: 1,
      url: '/api/settings/current/default',
    })
  }
})

app.post('/api/test', async function (req, res) {
  try {
    // api.clearOldFilesAndDB();

    res.send({
      message: "succes",
      status: 0,
      url: '/api/test',
    });
  } catch (error) {
    res.status(500).send({
      message: "error",
      status: 1,
      url: '/api/test',
    })
  }
})

// app.post('/api/convert', async function (req, res) {
//   try {
//     const file = path.join(__dirname, `../uploads/1100 Ð Ð (1).xls`);
//     const conv = new Converter(file);

//     res.send({ message: "OK" });
//   } catch (error) {
//     res.send({ message: "error" })
//   }
// })

// app.use(express.static(__dirname + "/public"));
// const staticPath = path.join(__dirname, '../../xls-pricefile-proc-front/build');
const staticPath = path.join(__dirname, '../static');
app.use(express.static(staticPath));
app.get('/*', async function (req, res) {
  res.sendFile(staticPath);
})

try {
  app.listen(PORT, () => {
    console.log("Server is running on port " + 8800);
  })
} catch (error) {
  console.log(error);
}
